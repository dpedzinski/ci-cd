require (
	github.com/aws/aws-lambda-go v1.16.0
	github.com/aws/aws-sdk-go v1.30.14
	github.com/awslabs/aws-lambda-go-api-proxy v0.6.0
	github.com/go-redis/cache v6.4.0+incompatible
	github.com/go-redis/redis v6.15.7+incompatible // indirect
	github.com/go-sql-driver/mysql v1.5.0
	github.com/gorilla/mux v1.7.4
	github.com/jinzhu/gorm v1.9.12
	github.com/pborman/uuid v1.2.0
	github.com/urfave/cli v1.22.1 // indirect
	golang.org/x/crypto v0.0.0-20191205180655-e7c4368fe9dd
	golang.org/x/oauth2 v0.0.0-20200107190931-bf48bf16ab8d
	gopkg.in/mgo.v2 v2.0.0-20180705113604-9856a29383ce
)

module hello-world

go 1.13
